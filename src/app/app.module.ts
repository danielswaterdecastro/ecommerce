import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AlertModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms';
import { GameStoreComponent } from './game-store/game-store.component';
import { CarrinhoComponent } from './carrinho-component/carrinho-component.component';
import { ProdutosComponent } from './produtos-component/produtos-component.component';
import { ProdutosService } from './service/produtos.service';

@NgModule({
  declarations: [
    GameStoreComponent,
    CarrinhoComponent,
    ProdutosComponent,    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    AlertModule.forRoot()
  ],
  providers: [
    ProdutosService,
    { provide: LOCALE_ID, useValue: "pt-BR" },
  ],
  bootstrap: [GameStoreComponent]
})
export class AppModule { }
