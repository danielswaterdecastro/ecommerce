import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ProdutosModel } from '../model/produtos.model';
/**
 * 
 * 
 * @export
 * @class CarrinhoComponent
 */
@Component({
  selector: 'carrinho-component',
  templateUrl: './carrinho-component.component.html',
  styleUrls: ['./carrinho-component.component.scss']
})
export class CarrinhoComponent {

  @Input('produto') produto;
  private showCard: boolean = true;
  private arrayProdutos: ProdutosModel[] = [];
  private qtdItem: string;
  private frete: number;
  private subtotal: number;
  private total: number;

  constructor() { }

  /**
  * 
  * 
  * @param {ProdutosModel} _produto 
  * @memberof CarrinhoComponent
  * Adiciona os produtos no carrinho
  */
  public adicionaItem(_produto: ProdutosModel) {
    this.subtotal = 0;
    this.frete = 0
    if (_produto) {
      this.showCard = false;
    }
    this.arrayProdutos.push(_produto);
    this.txtQuantidadeItemTotal()
    for (let i = 0; i < this.arrayProdutos.length; i++) {
      this.calculaValores(this.arrayProdutos[i].price, 1)
    }

  }

  /**
   * 
   * 
   * @private
   * @param {*} item 
   * @memberof CarrinhoComponent
   */
  private removeItem(item: ProdutosModel, index: any) {
    console.log(index)
    let preco = item.price
    this.arrayProdutos.splice(index, 1)
    this.calculaValores(preco, 2)
    this.txtQuantidadeItemTotal()
  }

  /**
  * 
  * 
  * @private
  * @param {number} valor 
  * @param {number} tipo 
  * @memberof CarrinhoComponent
  * Faz o calculo dos valores
  * tipo 1: Soma
  * tipo 2: Subtrai
  */
  private calculaValores(valor: number, tipo: number) {
    if (tipo == 1) {
      this.subtotal += valor;
      this.frete += 10.0
      this.total = this.subtotal + this.frete;
      if (this.subtotal > 300.0) {
        this.frete = 0.0;
      }
    }
    else {
      this.subtotal = this.subtotal - valor;
      if (this.subtotal < 300.0) {
        this.frete = this.arrayProdutos.length * 10.0
      }
      this.total = this.subtotal + this.frete;
      if (this.arrayProdutos.length == 0) {
        this.showCard = true;
      }
    }
  }

  /**
   * 
   * 
   * @private
   * @memberof CarrinhoComponent
   * Exibe o texto da quantidade total de itens
   */
  private txtQuantidadeItemTotal() {
    if (this.arrayProdutos.length == 1) {
      this.qtdItem = "(" + this.arrayProdutos.length + " item)";
    }
    else {
      this.qtdItem = "(" + this.arrayProdutos.length + " itens)";
    }
    if (this.arrayProdutos.length == 0) {
      this.qtdItem = '';
    }
  }
}
