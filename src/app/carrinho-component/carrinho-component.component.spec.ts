import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarrinhoComponentComponent } from './carrinho-component.component';

describe('CarrinhoComponentComponent', () => {
  let component: CarrinhoComponentComponent;
  let fixture: ComponentFixture<CarrinhoComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarrinhoComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarrinhoComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
