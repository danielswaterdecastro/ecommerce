import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { ProdutosModel } from '../model/produtos.model';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/**
 * 
 * 
 * @export
 * @class ProdutosService
 */

 const URL = '../../assets/produtos.json';

@Injectable()
export class ProdutosService {

    constructor(private http: Http) { }

    /**
     * Busca todos os produtos
     */
    public listaProdutos(): Observable<ProdutosModel[]>{
        return this.http.get(URL)
        .map(this.extractData)
        .catch(this.observableError)
    }


    /**
     * 
     * @param res 
     * Faz a extraçao do json
     */
    private extractData(res: Response){
        let body = res.json()
        return body || {}
    }


    /**
     * 
     * @param error 
     * Faz o tratamento do possível erro
     */
    private observableError(error: Response | any){
        return Observable.throw(error.message || error)
    }
    
}