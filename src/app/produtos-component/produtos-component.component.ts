import { Component, OnInit, ViewChild } from '@angular/core';
import { ProdutosService } from '../service/produtos.service';
import { ProdutosModel } from '../model/produtos.model';
import { Output } from '@angular/core/src/metadata/directives';
import 'rxjs/Rx';


@Component({
  selector: 'produtos-component',
  templateUrl: './produtos-component.component.html',
  styleUrls: ['./produtos-component.component.scss']
})
export class ProdutosComponent implements OnInit {
  
  public produtos: ProdutosModel[];
  public mensagemErro: string;
  public tipo: string;
  
  @ViewChild('carrinho') carrinho;
  
  constructor(private produtoService: ProdutosService) { }
  
  ngOnInit() {
    this.listaProdutos()
  }
  
  /**
  * Lista todos os produtos do arquivo .json
  */
  listaProdutos(): void{
    this.produtoService.listaProdutos()
    .subscribe(produtos => this.produtos = produtos,
      error => this.mensagemErro = <any>error,
    )
    
  }
  
  /**
  * 
  * @public
  * @param {*} prod 
  * @memberof ProdutosComponent
  * Passa o objeto produto para o componente do carrinho
  */
  public addCarrinho(prod: ProdutosModel){
    this.carrinho.adicionaItem(prod);
  }

  /**
   * 
   * 
   * @memberof ProdutosComponent
   */
  public filtraProduto(){
    this.produtos.sort(this.sortArray(this.tipo))
  }
  
  /**
  * 
  * 
  * @private
  * @param {*} key 
  * @memberof CarrinhoComponent
  * Organiza o array de produtos pela chave
  */
  private sortArray(key: any){
    return function(a,b){
      if(a[key] > b[key]){
        return 1
      }
      if(a[key] < b[key]){
        return -1
      }
    }
  }
}
