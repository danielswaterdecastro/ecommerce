import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdutoscComponentComponent } from './produtos-component.component';

describe('ProdutoscComponentComponent', () => {
  let component: ProdutoscComponentComponent;
  let fixture: ComponentFixture<ProdutoscComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdutoscComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdutoscComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
