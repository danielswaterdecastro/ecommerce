# Ecommerce

Teste realizado com o Angular CLI versão 1.2.7.

## Instalação do Angular CLI

Para rodar o projeto, é necessário a instalação do Angular CLI com a sua mais recente versão com o comando " npm install -g @angular/cli@latest "

## Instalando as dependências

Fazer o download das dependências do projeto com o comando " npm install "

## Testando o projeto

Após a instalação das dependências, utilizar o comando " ng serve "
Acessar o projeto na url http://localhost:4200/

## Observações

As imagens não possui fundo transparente, por isso não ficou fiel ao layout


